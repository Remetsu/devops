# DevOps summary 2
<pre>
Master v1.0	-v1.0--------------------------v1.1----------------  
Rel-cand v1.1	----v1.1------------------v1.1-------v1.2----------  
Dev 		-------O---------O------O--------------------------  
Feature		---------O----O------------------------------------  
</pre>

Masterista tehdään release-candi haara, jota kehitetään devi haarassa.
Rel-candiin laitetaan hot-fixit, jotka voidaan päivittää nopeasti masteriin.
